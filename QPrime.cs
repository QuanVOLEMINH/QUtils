﻿using System;

namespace QUtils
{
	public class QPrime
	{
		public static bool IsPrime(int n){
			if (n < 2 || n % 2 == 0)
			{
				return false;
			}
			if (n == 2 || n == 3)
			{
				return true;
			}
			for (var i = 3; i <= Math.Sqrt(n); i += 2)
			{
				if (n % i == 0)
				{
					return false;
				}
			}
			return true;
		}

	}
}
